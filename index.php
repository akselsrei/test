<?php

	class Products {

		var $name;
		var $price;
		var $size;
		var $height;
		var $width;
		var $lenght;
		var $weight;

		var $error;

		// Set

		function setProduct($name,$price,$size,$height,$width,$lenght,$weight) {

				$this->name = $name;
				$this->price = $price;

			if (empty($size)) {
				$this->size = null;
			}
			else {
				$this->size = $size;
			}

			if (empty($height)) {
				$this->height = null;
			}
			else {
				$this->height = $height;
			}

			if (empty($width)) {
				$this->width = null;
			}
			else {
				$this->width = $width;
			}

			if (empty($lenght)) {
				$this->lenght = null;
			}
			else {
				$this->lenght = $lenght;
			}

			if (empty($weight)) {
				$this->weight = null;
			}
			else {
				$this->weight = $weight;
			}
		}
		// END set

		function setError($error) {
			$this->error = $error;
		}

		function getError() {
			echo $this->error;
		}

		function addProductToDb()
		{

				$host = 'localhost';
			    $user = '';
			    $pass = '';
			    $name = '';

	   			$conn = mysqli_connect($host, $user, $pass, $name);



		  $query="INSERT INTO `products`(`name`, `price`, `size`, `height`, `width`, `lenght`, `weight`) VALUES ('$this->name','$this->price','$this->size','$this->height','$this->width','$this->lenght','$this->weight')";
		    
		    if (empty($this->size) && ( empty($this->height) || empty($this->width) || empty($this->lenght) ) && empty($this->weight)){
		    	$this->setError("Viens no tipiem ir jāizvēlas!");
		    }
		    else if ($conn->query($query) === TRUE) {
			    $this->setError("Produkts tika pievienots!");
			} else {
			    $this->setError("Oops,kaut kas nogāja greizi!");
			}

		}   
	}

	$product = new Products();

	if(isset($_POST['addproduct'])) {
		$product->setProduct($_POST['name'],$_POST['price'],$_POST['size'],$_POST['height'],$_POST['width'],$_POST['lenght'],$_POST['weight']);
		$product->addProductToDb();
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Scandiweb TEST</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script>
		$(function() {
		  $('#type').change(function(){
		    if ($('#type').val() == '1'){
		    	$('.output1').show();
		  	}
		  	else {
		  		$('.output1').hide();
		  	}

		  	if ($('#type').val() == '2'){
		    	$('.output2').show();
		  	}
		  	else {
		  		$('.output2').hide();
		  	}

		  	if ($('#type').val() == '3'){
		    	$('.output3').show();
		  	}
		  	else {
		  		$('.output3').hide();
		  	}
		  });
});
	</script>
</head>
<body class="bg-secondary">
	<div class="container">
		<div class="row d-flex justify-content-center mt-5">
			<div class="col-md-6 bg-white rounded px-5 py-5">
					 	<?php $product->getError(); ?>

					<h3>Pievienot produktu</h3><br>
					<form action="" method="post">
						<input type="text" name="name" class="form-control" placeholder="Ievadi preces nosaukumu" required>
						<br>
						<input type="text" name="price" class="form-control" placeholder="Ievadi preces cenu" required>
						<br>
						  <select class="custom-select" id="type">
						    <option selected>Izvēlies</option>
						    <option value="1">Size(inMB)forDVD-disc</option>
						    <option value="2">Weight(inKg)forBook</option>
						    <option value="3">Dimensions(HxWxL)forFurniture</option>
						  </select>
							<div class="output1"style="display:none; margin-top:50px;">
								Ievadi preces izmēru ( MB )
							  <input type="text" name="size" class="form-control" placeholder="Ievadi izmēru (MB)">
							</div>
							<div class="output2"style="display:none; margin-top:50px;">
								Ievadi precei svaru!
							  <input type="text" name="weight" class="form-control" placeholder="Ievadi svaru (KG)">
							</div>
							<div class="output3"style="display:none; margin-top:50px;">
								Ievadi precei parametrus!
							  <input type="text" name="height" class="form-control" placeholder="Ievadi augstumu">
							  <br>
							  <input type="text" name="lenght" class="form-control" placeholder="Ievadi platumu">
							  <br>
							  <input type="text" name="width" class="form-control" placeholder="Ievadi garumu">
							</div>
							<input type="submit" name="addproduct" class="btn btn-primary btn-lg btn-block mt-3" value="Pievienot!">
					</form>
			</div>
		</div>
					<a href="list.php"><button class="btn btn-info mt-5">Uz produktu sarakstu</button></a>
	</div>
</body>
</html>