<?php

	function printProducts() {

				$host = 'localhost';
			    $user = '';
			    $pass = '';
			    $name = '';

	   			$conn = mysqli_connect($host, $user, $pass, $name);

	   			$result = $conn->query("SELECT * FROM products");
	   			$num = $result->num_rows;

	   			if ($num == 0) {
	   				echo 'Nav neviena produkta!';
	   			}

				while ($row = mysqli_fetch_assoc($result)) {

					if ($row['size'] == null) {
						$row['size'] = '';
					}
					else {
						$row['size'] = '<li class="list-group-item">Izmērs (MB) '.$row['size'].'</li>';
					}

					if ($row['height'] == null) {
						$row['height'] = '';
					}
					else {
						$row['height'] = '<li class="list-group-item">Augstums '.$row['height'].'</li>';
					}

					if ($row['width'] == null) {
						$row['width'] = '';
					}
					else {
						$row['width'] = '<li class="list-group-item">Platums '.$row['width'].'</li>';
					}

					if ($row['lenght'] == null) {
						$row['lenght'] = '';
					}
					else {
						$row['lenght'] = '<li class="list-group-item">Garums '.$row['lenght'].'</li>';
					}

					if ($row['weight'] == null) {
						$row['weight'] = '';
					}
					else {
						$row['weight'] = '<li class="list-group-item">Svars KG '.$row['weight'].'</li>';
					}

		    		echo '				<div class="card mx-2" style="width: 18rem;">
							<div class="card-body">
								<h5 class="card-title">'.$row['name'].'</h5>
								<p class="card-text">'.$row['price'].' EUR</p>
							</div>
							<ul class="list-group list-group-flush">
								'.$row['size'].'
								'.$row['height'].'
								'.$row['width'].'
								'.$row['lenght'].'
								'.$row['weight'].'
							</ul>
						</div>';
				}
	}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Scandiweb TEST</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</head>
	<body class="bg-secondary">
		<div class="container">
			<div class="row d-flex justify-content-center mt-5">
				<?php printProducts(); ?>
			</div>
			<a href="index.php"><button class="btn btn-info mt-5">Uz sākumu</button></a>
		</div>
	</body>
</html>